# CMake generated Testfile for 
# Source directory: /home/jack/workspace/EliteQuant_Cpp/source
# Build directory: /home/jack/workspace/EliteQuant_Cpp/source/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("ThirdParty/zeromq")
subdirs("ThirdParty/cereal")
subdirs("ThirdParty/seasocks")
subdirs("EliteQuant")
subdirs("eqserver")
