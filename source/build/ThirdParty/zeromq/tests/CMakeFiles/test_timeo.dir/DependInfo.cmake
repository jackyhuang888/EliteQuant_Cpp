# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/tests/test_timeo.cpp" "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/tests/CMakeFiles/test_timeo.dir/test_timeo.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_GNU_SOURCE"
  "_REENTRANT"
  "_THREAD_SAFE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../ThirdParty/zeromq/tweetnacl/contrib/randombytes"
  "../ThirdParty/zeromq/tweetnacl/src"
  "../ThirdParty/zeromq/include"
  "ThirdParty/zeromq"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/CMakeFiles/libzmq.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
