# Install script for directory: /home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/libzmq.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xPerfToolsx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_lat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_lat")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_lat"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/bin/local_lat")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_lat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_lat")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_lat"
         OLD_RPATH "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_lat")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xPerfToolsx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_lat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_lat")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_lat"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/bin/remote_lat")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_lat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_lat")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_lat"
         OLD_RPATH "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_lat")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xPerfToolsx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_thr" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_thr")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_thr"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/bin/local_thr")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_thr" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_thr")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_thr"
         OLD_RPATH "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/local_thr")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xPerfToolsx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_thr" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_thr")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_thr"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/bin/remote_thr")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_thr" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_thr")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_thr"
         OLD_RPATH "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/remote_thr")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xPerfToolsx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_lat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_lat")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_lat"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/bin/inproc_lat")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_lat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_lat")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_lat"
         OLD_RPATH "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_lat")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xPerfToolsx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_thr" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_thr")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_thr"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/bin/inproc_thr")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_thr" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_thr")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_thr"
         OLD_RPATH "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/inproc_thr")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libzmq.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libzmq.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libzmq.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/lib/libzmq.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libzmq.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libzmq.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libzmq.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/include/zmq.h"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/include/zmq_utils.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/lib/libzmq-static.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/include/zmq.h"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/include/zmq_utils.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xSourceCodex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/src/zmq" TYPE FILE FILES
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/address.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/clock.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ctx.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/curve_client.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/curve_server.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/dealer.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/devpoll.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/dist.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/epoll.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/err.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/fq.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/io_object.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/io_thread.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ip.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ipc_address.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ipc_connecter.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ipc_listener.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/kqueue.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/lb.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/mailbox.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/mechanism.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/metadata.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/msg.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/mtrie.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/object.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/options.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/own.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/null_mechanism.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pair.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pgm_receiver.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pgm_sender.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pgm_socket.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pipe.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/plain_client.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/plain_server.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/poll.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/poller_base.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/precompiled.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/proxy.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pub.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pull.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/push.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/random.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/raw_encoder.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/raw_decoder.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/reaper.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/rep.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/req.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/router.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/select.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/session_base.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/signaler.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/socket_base.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/socks.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/socks_connecter.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/stream.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/stream_engine.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/sub.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tcp.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tcp_address.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tcp_connecter.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tcp_listener.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/thread.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/trie.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v1_decoder.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v1_encoder.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v2_decoder.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v2_encoder.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/xpub.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/xsub.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/zmq.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/zmq_utils.cpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/config.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/tweetnacl/src/tweetnacl.c"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/tweetnacl/contrib/randombytes/devurandom.c"
    "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/version.rc"
    "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/platform.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/address.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/array.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/atomic_counter.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/atomic_ptr.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/blob.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/clock.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/command.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/config.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ctx.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/curve_client.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/curve_server.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/dbuffer.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/dealer.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/decoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/devpoll.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/dist.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/encoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/epoll.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/err.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/fd.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/fq.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/gssapi_client.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/gssapi_mechanism_base.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/gssapi_server.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/i_decoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/i_encoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/i_engine.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/i_poll_events.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/io_object.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/io_thread.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ip.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ipc_address.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ipc_connecter.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ipc_listener.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/kqueue.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/lb.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/likely.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/mailbox.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/mechanism.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/metadata.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/msg.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/mtrie.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/mutex.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/norm_engine.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/null_mechanism.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/object.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/options.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/own.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pair.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pgm_receiver.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pgm_sender.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pgm_socket.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pipe.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/plain_client.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/plain_server.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/poll.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/poller.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/poller_base.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/precompiled.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/proxy.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pub.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/pull.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/push.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/random.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/raw_decoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/raw_encoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/reaper.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/rep.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/req.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/router.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/select.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/session_base.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/signaler.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/socket_base.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/socks.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/socks_connecter.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/stdint.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/stream.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/stream_engine.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/sub.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tcp.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tcp_address.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tcp_connecter.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tcp_listener.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/thread.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tipc_address.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tipc_connecter.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/tipc_listener.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/trie.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v1_decoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v1_encoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v2_decoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v2_encoder.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/v2_protocol.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/windows.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/wire.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/xpub.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/xsub.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ypipe.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ypipe_base.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/ypipe_conflate.hpp"
    "/home/jack/workspace/EliteQuant_Cpp/source/ThirdParty/zeromq/src/yqueue.hpp"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/etc/zmq" TYPE FILE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/AUTHORS.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/etc/zmq" TYPE FILE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/COPYING.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/etc/zmq" TYPE FILE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/COPYING.LESSER.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/etc/zmq" TYPE FILE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/MAINTAINERS.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/etc/zmq" TYPE FILE FILES "/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/NEWS.txt")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/jack/workspace/EliteQuant_Cpp/source/build/ThirdParty/zeromq/tests/cmake_install.cmake")

endif()

